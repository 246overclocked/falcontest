/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

public class Drivetrain extends SubsystemBase {
  private final int talonArrayLen = 4;

  private final int LEFT_MASTER_PORT = 5;
  private final int LEFT_SLAVE_PORT = 7;
  private final int RIGHT_MASTER_PORT = 6;
  private final int RIGHT_SLAVE_PORT = 8;

  private final NetworkTable falconTable = NetworkTableInstance.getDefault().getTable("falconTable");

  private int currentTalonIndex = -1;

  private boolean validCanId = false;

  WPI_TalonFX[] talons = new WPI_TalonFX[4];

  public Drivetrain() {
    talons[0] = new WPI_TalonFX(LEFT_MASTER_PORT);
    talons[1] = new WPI_TalonFX(LEFT_SLAVE_PORT);
    talons[2] = new WPI_TalonFX(RIGHT_MASTER_PORT);
    talons[3] = new WPI_TalonFX(RIGHT_SLAVE_PORT);

    for(int i = 0; i < talonArrayLen; i++) {
      talons[i].configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
      talons[i].setSensorPhase(true);
      talons[i].setNeutralMode(NeutralMode.Coast);
    }

    falconTable.getEntry("statorCurrent").setDouble(0);
    falconTable.getEntry("supplyCurrent").setDouble(0);

    falconTable.getEntry("canID").setNumber(0);

    falconTable.getEntry("rawOutput").setDouble(0);

    falconTable.getEntry("validCanID").setBoolean(false);

    falconTable.getEntry("rpm").setDouble(0);
  }

  private int findTalon(int deviceId) {
    for(int i = 0; i < this.talonArrayLen; i++) {
      if(this.talons[i].getDeviceID() == deviceId) {
        return i;
      }
    }
    return -1;
  }

  public void readTalonFromNT() {
    this.currentTalonIndex = findTalon(this.falconTable.getEntry("canID").getNumber(-1).intValue());
    if(this.currentTalonIndex > -1 && this.currentTalonIndex < this.talonArrayLen) {
      this.validCanId = true;
      this.falconTable.getEntry("validCanID").setBoolean(true);
      talons[this.currentTalonIndex].set(
        ControlMode.PercentOutput,
        this.falconTable.getEntry("rawOutput").getDouble(0));
    } else {
      this.validCanId = false;
      this.falconTable.getEntry("validCanID").setBoolean(false);
    }
    
  }

  public void writeTalonToNT() {
    if(this.validCanId) {
      this.falconTable.getEntry("statorCurrent")
        .setDouble(this.talons[this.currentTalonIndex]
        .getStatorCurrent());
      this.falconTable.getEntry("supplyCurrent")
        .setDouble(this.talons[this.currentTalonIndex]
        .getSupplyCurrent());
      this.falconTable.getEntry("rpm")
        .setDouble(this.talons[this.currentTalonIndex]
        .getSelectedSensorVelocity(0) * (600f/4096f));
    }
  }

  public void zeroOutputofAll()  {
    for(int i = 0; i < talonArrayLen; i++) {
      talons[i].set(ControlMode.PercentOutput, 0);
    }
  }

}
